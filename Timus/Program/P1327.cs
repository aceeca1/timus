using System;

class P1327 {
    public static void Main() {
        var a = int.Parse(Console.ReadLine());
        var b = int.Parse(Console.ReadLine());
        Console.WriteLine(((b + 1) >> 1) - (a >> 1));
    }
}
