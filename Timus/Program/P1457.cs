using System;
using System.Linq;

class P1457 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var ans = Console.ReadLine().Split().Average(k => int.Parse(k));
        Console.WriteLine(ans);
    }
}
