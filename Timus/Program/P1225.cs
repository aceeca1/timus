using System;
using System.Collections.Generic;
using System.Linq;

class P1225 {
    static IEnumerable<long> Fibonacci() {
        long a0 = 1L, a1 = 1L;
        for (;;) {
            yield return a0;
            long a2 = a0 + a1;
            a0 = a1;
            a1 = a2;
        }
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(Fibonacci().ElementAt(n - 1) << 1);
    }
}
