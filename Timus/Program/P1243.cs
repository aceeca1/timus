using System;
using System.Numerics;

class P1243 {
    public static void Main() {
        var n = BigInteger.Parse(Console.ReadLine());
        Console.WriteLine(n % 7);
    }
}
