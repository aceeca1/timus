using System;
using System.Collections.Generic;

class P1196 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new HashSet<int>();
        for (int i = 0; i < n; ++i) a.Add(int.Parse(Console.ReadLine()));
        var m = int.Parse(Console.ReadLine());
        int ans = 0;
        for (int i = 0; i < m; ++i)
            if (a.Contains(int.Parse(Console.ReadLine()))) ++ans;
        Console.WriteLine(ans);
    }
}
