using System;

class P1083 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = line[1].Length;
        int ans = 1;
        for (int i = n; i > 1; i -= k) ans *= i;
        Console.WriteLine(ans);
    }
}
