﻿using System;
using System.Collections.Generic;
using System.Linq;

class P1837 {
    const string Isenbaev = "Isenbaev";

    class ShortPathU : AShortPathU {
        protected override IEnumerable<int> E(int no) { return G.E[no]; }
        protected override int GetSizeV() { return G.E.Count; }
        protected override int GetSource() { return Source; }

        GraphV<string, int> G;
        int Source;

        public ShortPathU(GraphV<string, int> g, int source) {
            G = g;
            Source = source;
            Compute();
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var g = new GraphV<string, int>();
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var a0 = g.AddVertex(line[0]);
            var a1 = g.AddVertex(line[1]);
            var a2 = g.AddVertex(line[2]);
            g.E[a0].Add(a1);
            g.E[a0].Add(a2);
            g.E[a1].Add(a0);
            g.E[a1].Add(a2);
            g.E[a2].Add(a0);
            g.E[a2].Add(a1);
        }
        bool isIsenbaevPresent = g.V.ContainsKey(Isenbaev);
        var sp = new ShortPathU(g, g.AddVertex(Isenbaev));
        if (!isIsenbaevPresent) g.V.Remove(Isenbaev);
        foreach (var i in g.V.OrderBy(i => i.Key)) {
            Console.Write(i.Key);
            Console.Write(' ');
            var isenbaev = sp.Distance[i.Value];
            if (isenbaev == int.MaxValue) Console.WriteLine("undefined");
            else Console.WriteLine(isenbaev);
        }
    }
    
    // Snippet: GraphV
    class GraphV<Vertex, Edge> {
        public Dictionary<Vertex, int> V = new Dictionary<Vertex, int>();
        public List<List<Edge>> E = new List<List<Edge>>();

        public int AddVertex() {
            E.Add(new List<Edge>());
            return E.Count - 1;
        }

        public int AddVertex(Vertex label) {
            int n;
            if (V.TryGetValue(label, out n)) return n;
            V[label] = E.Count;
            return AddVertex();
        }
    }

    // Snippet: ShortPathU
    abstract class AShortPathU {
        protected abstract int GetSizeV();
        protected abstract IEnumerable<int> E(int no);
        protected abstract int GetSource();

        public int[] Distance, From;

        public void Compute() {
            Distance = new int[GetSizeV()];
            for (int i = 0; i < Distance.Length; ++i)
                Distance[i] = int.MaxValue;
            From = new int[GetSizeV()];
            var q = new Queue<int>();
            q.Enqueue(GetSource());
            Distance[GetSource()] = 0;
            while (q.Count != 0) {
                var qH = q.Dequeue();
                foreach (var i in E(qH))
                    if (Distance[i] == int.MaxValue) {
                        Distance[i] = Distance[qH] + 1;
                        From[i] = qH;
                        q.Enqueue(i);
                    }
            }
        }
    }
}
