﻿using System;
using System.Collections.Generic;

class P1683 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new List<int>();
        while (n != 1) {
            n = (n + 1) >> 1;
            a.Add(n);
        }
        Console.WriteLine(a.Count);
        Console.WriteLine(string.Join(" ", a));
    }
}
