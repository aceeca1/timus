using System;

class P2025 {
    public static void Main() {
        var t = int.Parse(Console.ReadLine());
        for (int i = 0; i < t; ++i) {
            var line = Console.ReadLine().Split();
            var n = (long)int.Parse(line[0]);
            var k = (long)int.Parse(line[1]);
            var a = n / k;
            var k1 = n % k;
            var k2 = k - k1;
            var ans1 = (a + 1) * (a + 1) * (k1 * (k1 - 1) >> 1);
            var ans2 = a * a * (k2 * (k2 - 1) >> 1);
            var ans3 = a * (a + 1) * k1 * k2;
            Console.WriteLine(ans1 + ans2 + ans3);
        }
    }
}
