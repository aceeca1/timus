﻿using System;
using System.Linq;

class P1290 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var ans = Enumerable.Range(0, n).Select(k => {
            return int.Parse(Console.ReadLine());
        }).OrderByDescending(k => k);
        foreach (var i in ans) Console.WriteLine(i);
    }
}
