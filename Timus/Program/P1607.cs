using System;

class P1607 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        var d = int.Parse(line[3]);
        var k = Math.Max(0, (c - a) / (b + d));
        a += k * b;
        c -= k * d;
        int ans;
        for (;;) {
            if (a >= c) { ans = a; break; }
            a += b;
            if (a >= c) { ans = c; break; }
            c -= d;
        }
        Console.WriteLine(ans);
    }
}
