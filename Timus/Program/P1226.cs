using System;
using System.Text.RegularExpressions;

class P1226 {
    static Regex Word = new Regex(@"[A-Za-z]+");
    
    static string Reverse(string s) {
        var a = s.ToCharArray();
        Array.Reverse(a);
        return new string(a);
    }
    
    static string Transform(string s) {
        return Word.Replace(s, k => Reverse(k.Value));
    }
    
    public static void Main() {
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            Console.WriteLine(Transform(line));
        }
    }
}
