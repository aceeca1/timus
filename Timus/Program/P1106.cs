using System;
using System.Collections.Generic;
using System.Linq;

class P1106 {
    class TryBipatite: ATryBipatite {
        protected override int GetSizeV() { return Edge.Length; }
        protected override IEnumerable<int> E(int no) { return Edge[no]; }
        
        public int[][] Edge;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var tb = new TryBipatite();
        tb.Edge = new int[n + 1][];
        tb.Edge[0] = new int[0];
        var isolated = false;
        for (int i = 1; i <= n; ++i) {
            var line = Console.ReadLine().Split();
            if (line.Length == 1) isolated = true;
            tb.Edge[i] = new int[line.Length - 1];
            for (int j = 0; j < tb.Edge[i].Length; ++j)
                tb.Edge[i][j] = int.Parse(line[j]);
        }
        if (isolated) Console.WriteLine(0);
        else {
            tb.Compute();
            var ans = Enumerable.Range(1, n).Where(k => tb.Label[k] == 0);
            Console.WriteLine(ans.Count());
            Console.WriteLine(String.Join(" ", ans));
        }
    }

    // Snippet: TryBipatite
    abstract class ATryBipatite {
        protected abstract int GetSizeV();
        protected abstract IEnumerable<int> E(int no);
    
        public bool Result;
        public int[] Label;
    
        public void Compute() {
            Result = true;
            Label = new int[GetSizeV()];
            for (int i = 0; i < GetSizeV(); ++i) Label[i] = -1;
            for (int i = 0; i < GetSizeV(); ++i) 
                if (Label[i] == -1) Visit(i, 0);
        }
        
        void Visit(int no, int labelno) {
            Label[no] = labelno;
            foreach (var i in E(no))
                if (Label[i] == -1) Visit(i, 1 - labelno);
                else if (Label[i] == labelno) Result = false;
        }
    }
}
