using System;

class P1545 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new string[n];
        for (int i = 0; i < n; ++i) a[i] = Console.ReadLine();
        var b = Console.ReadLine();
        for (int i = 0; i < n; ++i)
            if (a[i].StartsWith(b)) Console.WriteLine(a[i]);
    }
}
