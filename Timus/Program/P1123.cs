﻿using System;

class P1123 {
    public static void Main() {
        var result = new NextPalindrome(Console.ReadLine()).Result;
        Console.WriteLine(new string(result));
    }

    // Snippet: NextPalindrome
    class NextPalindrome {
        public char[] Result;

        public NextPalindrome(string input) {
            Result = new char[input.Length];
            var mid = (Result.Length - 1) >> 1;
            for (int i = 0; i <= mid; ++i)
                Result[i] = Result[Result.Length - 1 - i] = input[i];
            if (input.CompareTo(new string(Result)) <= 0) return;
            while (mid >= 0 && Result[mid] == '9') {
                Result[mid] = Result[Result.Length - 1 - mid] = '0';
                --mid;
            }
            Result[Result.Length - 1 - mid] = ++Result[mid];
        }
    }
}
