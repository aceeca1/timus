﻿using System;

class P1131 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        if (k > n - 1) k = n - 1;
        var c = 1;
        var ans = 0;
        while (c < k) { c += c; ++ans; }
        if (c < n) ans += (n - c + k - 1) / k;
        Console.WriteLine(ans);
    }
}
