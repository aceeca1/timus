using System;

class P1330 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n + 1];
        for (int i = 1; i <= n; ++i) a[i] = int.Parse(Console.ReadLine());
        for (int i = 1; i <= n; ++i) a[i] += a[i - 1];
        var q = int.Parse(Console.ReadLine());
        for (int i = 0; i < q; ++i) {
            var line = Console.ReadLine().Split();
            var s = int.Parse(line[0]);
            var t = int.Parse(line[1]);
            Console.WriteLine(a[t] - a[s - 1]);
        }
    }
}
