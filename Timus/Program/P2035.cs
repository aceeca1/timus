using System;

class P2035 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var x = int.Parse(line[0]);
        var y = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        if (c <= x) {
            Console.Write(c);
            Console.WriteLine(" 0");
        } else if (c <= x + y) {
            Console.Write(x);
            Console.Write(' ');
            Console.WriteLine(c - x);
        } else Console.WriteLine("Impossible");
    }
}
