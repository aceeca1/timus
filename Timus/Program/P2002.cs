﻿using System;
using System.Collections.Generic;

class P2002 {
    class User {
        public bool IsOnline;
        public string Password;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var db = new Dictionary<string, User>();
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            User user;
            switch (line[0]) {
                case "register":
                    if (db.ContainsKey(line[1]))
                        Console.WriteLine("fail: user already exists");
                    else {
                        db[line[1]] = new User { Password = line[2] };
                        Console.WriteLine("success: new user added");
                    }
                    break;
                case "login":
                    if (!db.TryGetValue(line[1], out user))
                        Console.WriteLine("fail: no such user");
                    else if (user.Password != line[2])
                        Console.WriteLine("fail: incorrect password");
                    else if (user.IsOnline)
                        Console.WriteLine("fail: already logged in");
                    else {
                        user.IsOnline = true;
                        Console.WriteLine("success: user logged in");
                    }
                    break;
                case "logout":
                    if (!db.TryGetValue(line[1], out user))
                        Console.WriteLine("fail: no such user");
                    else if (!user.IsOnline)
                        Console.WriteLine("fail: already logged out");
                    else {
                        user.IsOnline = false;
                        Console.WriteLine("success: user logged out");
                    }
                    break;
            }
        }
    }
}
