﻿using System;
using System.Collections.Generic;
using System.Linq;

class P1731 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        Console.WriteLine(String.Join(" ", Enumerable.Range(1, n)));
        Console.WriteLine(String.Join(" ", Range.Seq(n + 1, m, n)));
    }

    // Snippet: Range
    class Range {
        public static IEnumerable<int> Seq(int start, int length, int step) {
            int k = start;
            for (int i = 0; i < length; ++i) {
                yield return k;
                k += step;
            }
        }
    }
}
