﻿using System;
using System.Collections.Generic;
using System.Linq;

class P1118 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var s = int.Parse(line[0]);
        var t = int.Parse(line[1]);
        if (s == 1) Console.WriteLine(1);
        else {
            var min = double.PositiveInfinity;
            int arg = -1;
            for (int i = t; i >= s; --i) {
                var sum = Divisor.Of(i, new OpsInt()).Sum() - i;
                var trivial = (double)sum / i;
                if (trivial < min) { min = trivial; arg = i; }
                if (sum == 1) break;
            }
            Console.WriteLine(arg);
        }
    }

    // Snippet: Divisor
    class Divisor {
        public static IEnumerable<T> Of<T>(T n, IOps<T> ops) {
            var i = ops.One();
            for (; ops.Less(ops.Mul(i, i), n); i = ops.Inc(i))
                if (ops.IsZero(ops.Mod(n, i))) {
                    yield return i;
                    yield return ops.Div(n, i);
                }
            if (ops.Eq(ops.Mul(i, i), n)) yield return i;
        }
    }

    // Snippet: IOps
    interface IOps<T> {
        T From(int a);
        T Zero();
        T One();
        T MinusOne();
        T MinValue();
        T MaxValue();

        bool IsEven(T a);
        bool IsOne(T a);
        bool IsZero(T a);
        int Sign(T a);

        T Inc(T a);
        T Dec(T a);
        T Abs(T a);
        T Add(T a1, T a2);
        T Sub(T a1, T a2);
        T Mul(T a1, T a2);
        T Div(T a1, T a2);
        T Mod(T a1, T a2);
        T Shl(T a1, int a2);
        T Shr(T a1, int a2);
        T Max(T a1, T a2);
        T Min(T a1, T a2);

        bool Less(T a1, T a2);
        bool Eq(T a1, T a2);
    }

    // Snippet: OpsInt
    class OpsInt : IOps<int> {
        public int From(int a) { return a; }
        public int Zero() { return 0; }
        public int One() { return 1; }
        public int MinusOne() { return -1; }
        public int MinValue() { return int.MinValue; }
        public int MaxValue() { return int.MaxValue; }

        public bool IsEven(int a) { return (a & 1) == 0; }
        public bool IsOne(int a) { return a == 1; }
        public bool IsZero(int a) { return a == 0; }
        public int Sign(int a) { return a; }

        public int Inc(int a) { return a + 1; }
        public int Dec(int a) { return a - 1; }
        public int Abs(int a) { return Math.Abs(a); }
        public int Add(int a1, int a2) { return a1 + a2; }
        public int Sub(int a1, int a2) { return a1 - a2; }
        public int Mul(int a1, int a2) { return a1 * a2; }
        public int Div(int a1, int a2) { return a1 / a2; }
        public int Mod(int a1, int a2) { return a1 % a2; }
        public int Shl(int a1, int a2) { return a1 << a2; }
        public int Shr(int a1, int a2) { return a2 >> a2; }
        public int Max(int a1, int a2) { return Math.Max(a1, a2); }
        public int Min(int a1, int a2) { return Math.Min(a1, a2); }

        public bool Less(int a1, int a2) { return a1 < a2; }
        public bool Eq(int a1, int a2) { return a1 == a2; }
    }
}
