using System;

class P1409 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        Console.Write(b - 1);
        Console.Write(' ');
        Console.WriteLine(a - 1);
    }
}
