﻿using System;

class P2020 {
    public static void Main() {
        var s1 = Console.ReadLine();
        var s2 = Console.ReadLine();
        int i1 = 0, i2 = 0, ans = 0;
        while (i1 < s1.Length && i2 < s2.Length) {
            bool b1 = s1[i1] == 'L';
            bool b2 = s2[i2] == 'L';
            if (b1 && !b2) ++i2;
            else if (b2 && !b1) ++i1;
            else { ++i1; ++i2; }
            ++ans;
        }
        var ans1 = s1.Length - i1;
        var ans2 = s2.Length - i2;
        Console.WriteLine(ans + ans1 + ans2);
    }
}
