﻿using System;

class P1203 {
    class Report {
        public int Start, End;
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Report[n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            a[i] = new Report {
                Start = int.Parse(line[0]),
                End = int.Parse(line[1])
            };
        }
        Array.Sort(a, (k1, k2) => {
            if (k1.Start < k2.Start) return -1;
            if (k1.Start > k2.Start) return 1;
            return k2.End.CompareTo(k1.End);
        });
        var z = 0;
        for (int i = 1; i < n; ++i) {
            while (z >= 0 && a[i].End <= a[z].End) --z;
            a[++z] = a[i];
        }
        var ans = 0;
        var cEnd = -1;
        for (int i = 0; i <= z; ++i) {
            if (a[i].Start - cEnd < 1) continue;
            cEnd = a[i].End;
            ++ans;
        }
        Console.WriteLine(ans);
    }
}
