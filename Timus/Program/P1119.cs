﻿using System;
using System.Collections.Generic;
using System.Linq;

class P1119 {
    class Coordinate {
        public int X, Y;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var k = int.Parse(Console.ReadLine());
        var a = Enumerable.Range(0, k).Select(k1 => {
            line = Console.ReadLine().Split();
            var x = int.Parse(line[0]);
            var y = int.Parse(line[1]);
            return new Coordinate { X = x, Y = y };
        }).OrderBy(k1 => k1.X).ThenByDescending(k1 => k1.Y)
            .Select(k1 => k1.Y).ToArray();
        var len = new LongestIncreasingSubsequence<int>(a, new OpsInt())
            .Result.Length;
        var ans = (len * Math.Sqrt(2.0) + (n + m - len - len)) * 100.0;
        Console.WriteLine(ans.ToString("F0"));
    }

    // Snippet: LongestIncreasingSubsequence
    class LongestIncreasingSubsequence<T> {
        public T[] Input;
        public IOps<T> Ops;
        public int[] Result;
        public IEnumerable<T> ResultValue;

        public LongestIncreasingSubsequence(T[] input, IOps<T> ops) {
            Input = input;
            Ops = ops;
            Compute();
            ResultValue = Result.Select(k => Input[k]);
        }

        void Compute() {
            var b = new T[Input.Length + 1];
            for (int i = 0; i < b.Length; ++i) b[i] = Ops.MaxValue();
            var aFrom = new int[Input.Length];
            var bFrom = new int[b.Length];
            b[0] = Ops.MinValue();
            bFrom[0] = -1;
            for (int i = 0; i < Input.Length; ++i) {
                var k = BSearch.Int(
                    0, b.Length, 
                    kk => !Ops.Less(b[kk], Input[i])
                );
                b[k] = Input[i];
                bFrom[k] = i;
                aFrom[i] = bFrom[k - 1];
            }
            var len = BSearch.Int(
                0, b.Length,
                k => Ops.Eq(b[k], Ops.MaxValue())
            ) - 1;
            Result = new int[len];
            if (len > 0) {
                var u = bFrom[len];
                Result[0] = u;
                for (int i = 1; i < len; ++i) Result[i] = u = aFrom[u];
                Array.Reverse(Result);
            }
        }
    }

    // Snippet: BSearch
    class BSearch {
        public static int Int(int s, int t, Func<int, bool> f) {
            for (;;) {
                if (s == t) return s;
                int m = s + ((t - s) >> 1);
                if (f(m)) t = m; else s = m + 1;
            }
        }
    }

    // Snippet: IOps
    interface IOps<T> {
        T From(int a);
        T Zero();
        T One();
        T MinusOne();
        T MinValue();
        T MaxValue();

        bool IsEven(T a);
        bool IsOne(T a);
        bool IsZero(T a);
        int Sign(T a);

        T Inc(T a);
        T Dec(T a);
        T Abs(T a);
        T Add(T a1, T a2);
        T Sub(T a1, T a2);
        T Mul(T a1, T a2);
        T Div(T a1, T a2);
        T Mod(T a1, T a2);
        T Shl(T a1, int a2);
        T Shr(T a1, int a2);
        T Max(T a1, T a2);
        T Min(T a1, T a2);

        bool Less(T a1, T a2);
        bool Eq(T a1, T a2);
    }

    // Snippet: OpsInt
    class OpsInt : IOps<int> {
        public int From(int a) { return a; }
        public int Zero() { return 0; }
        public int One() { return 1; }
        public int MinusOne() { return -1; }
        public int MinValue() { return int.MinValue; }
        public int MaxValue() { return int.MaxValue; }

        public bool IsEven(int a) { return (a & 1) == 0; }
        public bool IsOne(int a) { return a == 1; }
        public bool IsZero(int a) { return a == 0; }
        public int Sign(int a) { return a; }

        public int Inc(int a) { return a + 1; }
        public int Dec(int a) { return a - 1; }
        public int Abs(int a) { return Math.Abs(a); }
        public int Add(int a1, int a2) { return a1 + a2; }
        public int Sub(int a1, int a2) { return a1 - a2; }
        public int Mul(int a1, int a2) { return a1 * a2; }
        public int Div(int a1, int a2) { return a1 / a2; }
        public int Mod(int a1, int a2) { return a1 % a2; }
        public int Shl(int a1, int a2) { return a1 << a2; }
        public int Shr(int a1, int a2) { return a2 >> a2; }
        public int Max(int a1, int a2) { return Math.Max(a1, a2); }
        public int Min(int a1, int a2) { return Math.Min(a1, a2); }

        public bool Less(int a1, int a2) { return a1 < a2; }
        public bool Eq(int a1, int a2) { return a1 == a2; }
    }
}
