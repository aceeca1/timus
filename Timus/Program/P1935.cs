using System;
using System.Linq;

class P1935 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var line = Console.ReadLine().Split();
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(line[i]);
        Array.Sort(a);
        Console.WriteLine(a.Sum() + a[a.Length - 1]);
    }
}
