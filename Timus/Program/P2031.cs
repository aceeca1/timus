using System;
using System.Linq;

class P2031 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        if (n <= 4) {
            var ans = new string[] { "16", "06", "68", "88" };
            Console.WriteLine(String.Join(" ", ans.Take(n)));
        } else Console.WriteLine("Glupenky Pierre");
    }
}
