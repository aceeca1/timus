using System;

class P1136 {
    static void Translate(int[] a, int start, int length) {
        if (length == 0) return;
        var pivot = a[start + length - 1];
        var u = BSearch.Int(start, start + length - 1, k => pivot < a[k]);
        Translate(a, u, start + length - 1 - u);
        Translate(a, start, u - start);
        Console.WriteLine(pivot);
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        Translate(a, 0, a.Length);
    }
    
    // Snippet: BSearch
    class BSearch {
        public static int Int(int s, int t, Func<int, bool> f) {
            for (;;) {
                if (s == t) return s;
                int m = s + ((t - s) >> 1);
                if (f(m)) t = m; else s = m + 1;
            }
        }
    }
}
