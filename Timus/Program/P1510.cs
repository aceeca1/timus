﻿using System;

class P1510 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int element = -1, count = 0;
        for (int i = 0; i < n; ++i) {
            var k = int.Parse(Console.ReadLine());
            if (count == 0) {
                count = 1;
                element = k;
            } else if (k == element) ++count; else --count;
        }
        Console.WriteLine(element);
    }
}
