using System;

class P1263 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        var a = new int[n + 1];
        for (int i = 0; i < m; ++i) ++a[int.Parse(Console.ReadLine())];
        for (int i = 1; i <= n; ++i)
            Console.WriteLine(((double)a[i] / m).ToString("P2"));
    }
}
