﻿using System;
using System.Linq;

class P1786 {
    static int Distance(char a1, char a2) {
        int ans = 0;
        if (Char.IsLower(a1) != Char.IsLower(a2)) ans += 5;
        if (Char.ToLower(a1) != Char.ToLower(a2)) ans += 5;
        return ans;
    }

    static int Distance(string a1, string a2) {
        return Enumerable.Range(0, 6).Sum(k => Distance(a1[k], a2[k]));
    }

    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(Enumerable.Range(0, s.Length - 5).Select(k =>
            Distance(s.Substring(k, 6), "Sandro")
        ).Min());
    }
}
