using System;

class P1353 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new NumbersGivenDigitSum(9, n).Results;
        Console.WriteLine(n == 1 ? a[n] + 1 : a[n]);
    }
    
    // Snippet: NumbersGivenDigitSum
    class NumbersGivenDigitSum {
        public int[] Results;
        
        public NumbersGivenDigitSum(int k, int n = -1) {
            if (n == -1) n = k * 9;
            Results = new int[n + 1];
            Results[0] = 1;
            for (int i = 1; i <= k; ++i) {
                var rNew = new int[n + 1];
                rNew[0] = Results[0];
                for (int j = 1; j <= n; ++j) {
                    var aX = j >= 10 ? Results[j - 10] : 0;
                    rNew[j] = rNew[j - 1] - aX + Results[j];
                }
                Results = rNew;
            }
        }
    }
}
