﻿using System;
using System.Linq;

class P1723 {
    public static void Main() {
        var s = Console.ReadLine();
        var a = new int[256];
        foreach (var i in s) ++a[i];
        var m = a.Max();
        Console.WriteLine((char)a.TakeWhile(k => k != m).Count());
    }
}
