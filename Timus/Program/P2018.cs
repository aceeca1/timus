using System;
using System.Collections.Generic;

class P2018 {
    const int M = 1000000007;
    
    class QueueSum {
        Queue<int> Q = new Queue<int>();
        public int Sum;
        
        public QueueSum(int k) {
            for (int i = 0; i < k; ++i) Q.Enqueue(0);
        }
        
        public void Enqueue(int k) {
            Sum += M - Q.Dequeue();
            if (Sum >= M) Sum -= M;
            Sum += k;
            if (Sum >= M) Sum -= M;
            Q.Enqueue(k);
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var a = int.Parse(line[1]);
        var b = int.Parse(line[2]);
        var kA = new QueueSum(a); kA.Enqueue(1);
        var kB = new QueueSum(b); kB.Enqueue(1);
        for (int i = 2; i <= n; ++i) {
            int sumA = kA.Sum, sumB = kB.Sum;
            kA.Enqueue(sumB);
            kB.Enqueue(sumA);
        }
        Console.WriteLine((kA.Sum + kB.Sum) % M);
    }
}
