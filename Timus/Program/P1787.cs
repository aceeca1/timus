using System;

class P1787 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var k = int.Parse(line[0]);
        var n = int.Parse(line[1]);
        line = Console.ReadLine().Split();
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            ans += int.Parse(line[i]);
            ans -= k;
            if (ans < 0) ans = 0;
        }
        Console.WriteLine(ans);
    }
}
