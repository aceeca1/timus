using System;
using System.Linq;

class P1082 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var range = Enumerable.Range(1, n);
        Console.WriteLine(String.Join(" ", range));
    }
}
