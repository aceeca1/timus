﻿using System;
using System.Linq;

class P1104 {
    public static void Main() {
        var s = Console.ReadLine().Select(k1 => {
            if ('0' <= k1 && k1 <= '9') return k1 - '0';
            return k1 - 'A' + 10;
        });
        var sum = s.Sum();
        var k = Math.Max(2, s.Max() + 1);
        while (k <= 36 && sum % (k - 1) != 0) ++k;
        if (k == 37) Console.WriteLine("No solution.");
        else Console.WriteLine(k);
    }
}
