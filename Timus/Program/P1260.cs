using System;

class P1260 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        long a1 = 1L, a2 = 1L, a3 = 2L;
        for (int i = 2; i <= n; ++i) {
            var a4 = a3 + a1 + 1L;
            a1 = a2;
            a2 = a3;
            a3 = a4;
        }
        Console.WriteLine(a1);
    }
}
