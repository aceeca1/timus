using System;

class P1639 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var m = int.Parse(line[0]);
        var n = int.Parse(line[1]);
        if ((m & 1) == 0 || (n & 1) == 0) 
            Console.WriteLine("[:=[first]");
        else Console.WriteLine("[second]=:]");
    }
}
