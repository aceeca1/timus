using System;

class P1644 {
    public static void Main() {
        int maxHungry = 2, minSatisfied = 10;
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var num = int.Parse(line[0]);
            var status = line[1];
            switch (status) {
                case "hungry":
                    maxHungry = Math.Max(maxHungry, num); break;
                case "satisfied":
                    minSatisfied = Math.Min(minSatisfied, num); break;
            }
        }
        if (minSatisfied <= maxHungry) Console.WriteLine("Inconsistent");
        else Console.WriteLine(minSatisfied);
    }
}
