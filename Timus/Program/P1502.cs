using System;

class P1502 {
    public static void Main() {
        var n = long.Parse(Console.ReadLine());
        Console.WriteLine((n * (n + 1) >> 1) * (n + 2));
    }
}
