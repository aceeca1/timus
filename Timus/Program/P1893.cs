using System;

class P1893 {
    static string Solve(int row, char column) {
        if (row < 3)
            switch (column) {
                case 'A': case 'D': return "window";
                default: return "aisle";
            }
        else if (row < 21)
            switch (column) {
                case 'A': case 'F': return "window";
                default: return "aisle";
            }
        else
            switch (column) {
                case 'A': case 'K': return "window";
                case 'C': case 'D': case 'G': case 'H': return "aisle";
                default: return "neither";
            }
    }
    
    
    public static void Main() {
        var seat = Console.ReadLine();
        var row = int.Parse(seat.Substring(0, seat.Length - 1));
        var column = seat[seat.Length - 1];
        Console.WriteLine(Solve(row, column));
    }
}
