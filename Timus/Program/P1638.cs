using System;

class P1638 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var content = int.Parse(line[0]);
        var cover = int.Parse(line[1]);
        var start = int.Parse(line[2]);
        var end = int.Parse(line[3]);
        var w = cover + content + cover;
        start = start * w + cover + content;
        end = end * w + cover;
        Console.WriteLine(Math.Abs(end - start));
    }
}
