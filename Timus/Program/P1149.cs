﻿using System;

class P1149 {
    static void PrintA(int n, int k = 1) {
        if (n == k) {
            Console.Write("sin(");
            Console.Write(n);
            Console.Write(')');
        } else {
            Console.Write("sin(");
            Console.Write(k);
            Console.Write((k & 1) == 0 ? '+' : '-');
            PrintA(n, k + 1);
            Console.Write(')');
        }
    }

    static void PrintS(int n, int k = 1) {
        if (n == k) {
            PrintA(1);
            Console.Write('+');
            Console.Write(n);
        } else {
            Console.Write('(');
            PrintS(n, k + 1);
            Console.Write(')');
            PrintA(n + 1 - k);
            Console.Write('+');
            Console.Write(k);
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        PrintS(n);
        Console.WriteLine();
    }
}
