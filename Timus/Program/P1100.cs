using System;
using System.Collections.Generic;
using System.Linq;

class P1100 {
    class Team {
        public int Id, M;
    }
    
    static IEnumerable<Team> ReadAll(int n) {
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            yield return new Team { 
                Id = int.Parse(line[0]), M = int.Parse(line[1]) 
            };
        }
    } 
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        foreach (var i in ReadAll(n).OrderByDescending(k => k.M)) {
            Console.Write(i.Id);
            Console.Write(' ');
            Console.WriteLine(i.M);
        }
    }
}
