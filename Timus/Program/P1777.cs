using System;
using System.Collections.Generic;

class P1777 {
    static List<long> A = new List<long>();
    static long MinDifference = long.MaxValue;

    static void AddToA(long x) {
        foreach (var i in A) 
            MinDifference = Math.Min(MinDifference, Math.Abs(i - x));
        A.Add(x);
    }
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        for (int i = 0; i < 3; ++i) AddToA(long.Parse(line[i]));
        while (MinDifference != 0) AddToA(MinDifference);
        Console.WriteLine(A.Count - 2);
    }
}
