﻿using System;
using System.Linq;

class P1005 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var line = Console.ReadLine().Split();
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(line[i]);
        var sum = a.Sum();
        var half = sum >> 1;
        var can = new bool[half + 1];
        can[0] = true;
        for (int i = 0; i < n; ++i)
            for (int j = half; j >= a[i]; --j)
                if (can[j - a[i]]) can[j] = true;
        while (!can[half]) --half;
        Console.WriteLine(sum - half - half);
    }
}
