﻿using System;
using System.Collections.Generic;
using System.Linq;

class P1711 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new string[n][];
        for (int i = 0; i < n; ++i) {
            a[i] = Console.ReadLine().Split();
            Array.Sort(a[i]);
        }
        var b = new int[n];
        var line = Console.ReadLine().Split();
        for (int i = 0; i < n; ++i) b[i] = int.Parse(line[i]) - 1;
        try {
            var ans = new List<string>();
            string c = "";
            for (int i = 0; i < n; ++i) {
                c = a[b[i]].First(k => c.CompareTo(k) < 0);
                ans.Add(c);
            }
            foreach (var i in ans) Console.WriteLine(i);
        } catch (InvalidOperationException) {
            Console.WriteLine("IMPOSSIBLE");
        }
    }
}
