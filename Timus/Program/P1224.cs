using System;

class P1224 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = long.Parse(line[0]);
        var m = long.Parse(line[1]);
        Console.WriteLine(n <= m ? n + n - 2 : m + m - 1);
    }
}
