using System;

class P1370 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]) % n;
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        for (int i = 0; i < 10; ++i) {
            Console.Write(a[m++]);
            if (a.Length <= m) m = 0;
        }
        Console.WriteLine();
    }
}
