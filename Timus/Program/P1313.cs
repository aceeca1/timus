using System;
using System.Collections.Generic;

class P1313 {
    static IEnumerable<int> Diag(int[,] a) {
        int maxX = a.GetLength(0) - 1;
        int maxY = a.GetLength(1) - 1;
        for (int i = 0; i <= maxX + maxY; ++i) {
            int lbY = Math.Max(0, i - maxX);
            int ubY = Math.Min(maxY, i);
            for (int y = lbY; y <= ubY; ++y) yield return a[i - y, y];
        }
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n, n];
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            for (int j = 0; j < n; ++j) a[i, j] = int.Parse(line[j]);
        }
        Console.WriteLine(String.Join(" ", Diag(a)));
    }
}
