using System;
using System.Linq;

class P1493 {
    static bool Lucky(int n) {
        var s = n.ToString("D6");
        var s1 = s.Take(3).Sum(k => k - '0');
        var s2 = s.Skip(3).Sum(k => k - '0');
        return s1 == s2;
    }
    
    public static void Main() {
        var a = int.Parse(Console.ReadLine());
        Console.WriteLine(Lucky(a - 1) || Lucky(a + 1) ? "Yes" : "No");
    }
}
