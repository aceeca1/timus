using System;

class P1902 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var t = int.Parse(line[1]);
        var s = int.Parse(line[2]);
        foreach (var i in Console.ReadLine().Split()) {
            var ii = int.Parse(i);
            Console.WriteLine((double)(ii + s + t) * 0.5);
        }
    }
}
