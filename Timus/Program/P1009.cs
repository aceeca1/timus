using System;
using System.Collections.Generic;
using System.Linq;

class P1009 {
    static IEnumerable<long> F(int k) {
        long a0 = 1, a1 = k - 1;
        for (;;) {
            yield return a0;
            var a2 = (a0 + a1) * (k - 1);
            a0 = a1;
            a1 = a2;
        }
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var k = int.Parse(Console.ReadLine());
        Console.WriteLine(F(k).ElementAt(n));
    }
}
