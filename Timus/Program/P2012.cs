using System;

class P2012 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(n >= 7 ? "YES" : "NO");
    }
}
