﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

class P1297 {
    public static void Main() {
        var s = new ReadOnlyList.FromString(Console.ReadLine());
        var result = new LongestPalindrome<char>(
            s, (k1, k2) => k1 != k2
        ).Result;
        Console.WriteLine(String.Concat(result));
    }

    // Snippet: LongestPalindrome
    class LongestPalindrome<T> {
        public int Start, Length;
        public IEnumerable<T> Result;

        public LongestPalindrome(
            IReadOnlyList<T> input, Func<T, T, bool> neq 
        ) {
            var a = new int[(input.Count << 1) + 1];
            int c = 0;
            for (int i = 0; i < a.Length; ++i) {
                int t = c + a[c] - i;
                if (t > 0) t = Math.Min(t, a[c + c - i]);
                else t = 0;
                for (;; ++t) {
                    int p = i - 1 - t, q = i + 1 + t;
                    if (p < 0 || q >= a.Length) break;
                    if ((p & 1) != 0)
                        if (neq(input[p >> 1], input[q >> 1])) break;
                }
                a[i] = t;
                if (i + a[i] > c + a[c]) c = i;
                if (t > Length) {
                    Length = t;
                    Start = (i + 1 - t) >> 1;
                }
            }
            Result = input.Skip(Start).Take(Length);
        }
    }

    // Snippet: ReadOnlyList
    class ReadOnlyList {
        public class FromString : IReadOnlyList<char> {
            public char this[int index] { get { return S[index]; } }
            public int Count { get { return S.Length; } }
            public string S;
            public FromString(string s) { S = s; }
            public IEnumerator<char> GetEnumerator() {
                return S.GetEnumerator();
            }
            IEnumerator IEnumerable.GetEnumerator() {
                return S.GetEnumerator();
            }
        }
    }
}
