using System;
using System.Linq;

class P1567 {
    static string[] Dial = {
        ".,!", 
        "abc", "def", "ghi",
        "jkl", "mno", "pqr",
        "stu", "vwx", "yz", 
    };
    static int[] NumDial = new int[256];
    
    public static void Main() {
        for (int i = 0; i <= 9; ++i)
            for (int j = 0; j < Dial[i].Length; ++j)
                NumDial[Dial[i][j]] = j + 1;
        NumDial[' '] = 1;
        Console.WriteLine(Console.ReadLine().Select(k => NumDial[k]).Sum());
    }
}
