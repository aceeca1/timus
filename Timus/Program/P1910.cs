using System;

class P1910 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var line = Console.ReadLine().Split();
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(line[i]);
        int max = 0;
        int arg = 0;
        for (int i = 1; i < n - 1; ++i) {
            var s = a[i - 1] + a[i] + a[i + 1];
            if (s > max) {
                max = s;
                arg = i;
            }
        }
        Console.Write(max);
        Console.Write(' ');
        Console.WriteLine(arg + 1);
    }
}
