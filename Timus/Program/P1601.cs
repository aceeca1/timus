﻿using System;

class P1601 {
    public static void Main() {
        bool head = true;
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            foreach (var i in line)
                if (Char.IsLetter(i)) {
                    Console.Write(head ? Char.ToUpper(i) : Char.ToLower(i));
                    head = false;
                } else {
                    Console.Write(i);
                    if (i == '.' || i == '?' || i == '!') head = true;
                }
            Console.WriteLine();
        }
    }
}
