﻿using System;
using System.Linq;

class P2068 {
    public static void Main() {
        Console.ReadLine();
        var a = Console.ReadLine().Split().Sum(k => int.Parse(k) - 1);
        Console.WriteLine((a & 2) == 0 ? "Stannis" : "Daenerys");
    }
}
