using System;

class P1877 {
    public static void Main() {
        var n1 = int.Parse(Console.ReadLine());
        var n2 = int.Parse(Console.ReadLine());
        Console.WriteLine((n1 & 1) == 0 || (n2 & 1) != 0 ? "yes" : "no");
    }
}
