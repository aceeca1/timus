using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class P1712 {
    static IEnumerable<char> Decipher(char[,] cipher, char[,] data) {
        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j)
                if (cipher[i, j] == 'X')
                    yield return data[i, j];
    }
    
    static char[,] RotateClockwise(char[,] board) {
        var ret = new char[4, 4];
        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j)
                ret[j, 3 - i] = board[i, j];
        return ret;
    }
    
    static char[,] ReadBoard() {
        var ret = new char[4, 4];
        for (int i = 0; i < 4; ++i) {
            var line = Console.ReadLine();
            for (int j = 0; j < 4; ++j) ret[i, j] = line[j];
        }
        return ret;
    }
    
    public static void Main() {
        var cipher = ReadBoard();
        var data = ReadBoard();
        var ans = new StringBuilder();
        for (int i = 0; i < 4; ++i) {
            foreach (var j in Decipher(cipher, data)) ans.Append(j);
            cipher = RotateClockwise(cipher);
        }
        Console.WriteLine(ans);
    }    
}
