using System;

class P1688 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = long.Parse(line[0]) * 3;
        var m = long.Parse(line[1]);
        int i = 0;
        for (; i < m; ++i) {
            n -= long.Parse(Console.ReadLine());
            if (n < 0) break;
        }
        Console.WriteLine(i == m 
            ? "Team.GOV!" 
            : $"Free after {i + 1} times."
        );
    }
}
