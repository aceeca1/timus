using System;
using System.Collections.Generic;

class P1001 {
    public static void Main() {
        var ans = new List<double>();
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            foreach (var i in line.Split()) {
                if (i.Length == 0) continue;
                ans.Add(Math.Sqrt(double.Parse(i)));
            }
        }
        for (int i = ans.Count - 1; i >= 0; --i)
            Console.WriteLine(ans[i].ToString("F4"));
    }
}
