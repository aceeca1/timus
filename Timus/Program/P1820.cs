using System;

class P1820 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        Console.WriteLine(Math.Max(2, (n + n + k - 1) / k));
    }
}
