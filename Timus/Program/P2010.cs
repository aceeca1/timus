using System;

class P2010 {
    static int[] KingX = { 0, -1, -1, -1,  0,  1, 1, 1 }; 
    static int[] KingY = { 1,  1,  0, -1, -1, -1, 0, 1 };
    static int[] KnightX = { -1, -2, -2, -1,  1,  2, 2, 1 };
    static int[] KnightY = {  2,  1, -1, -2, -2, -1, 1, 2 };
    
    static int ValidMove(int[] dX, int[] dY, int n, int x, int y) {
        int ans = 0;
        for (int i = 0; i < dX.Length; ++i) {
            int cX = dX[i] + x;
            int cY = dY[i] + y;
            if (1 <= cX && cX <= n && 1 <= cY && cY <= n) ++ans;
        }
        return ans;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var line = Console.ReadLine().Split();
        var x = int.Parse(line[0]);
        var y = int.Parse(line[1]);
        Console.Write("King: ");
        Console.WriteLine(ValidMove(KingX, KingY, n, x, y));
        Console.Write("Knight: ");
        Console.WriteLine(ValidMove(KnightX, KnightY, n, x, y));
        var bishop1 = Math.Min(x - 1, y - 1);
        var bishop2 = Math.Min(n - x, n - y);
        var bishop3 = Math.Min(x - 1, n - y);
        var bishop4 = Math.Min(n - x, y - 1);
        var bishop = bishop1 + bishop2 + bishop3 + bishop4;
        Console.Write("Bishop: ");
        Console.WriteLine(bishop);
        var rook = (n - 1) << 1;
        Console.Write("Rook: ");
        Console.WriteLine(rook);
        Console.Write("Queen: ");
        Console.WriteLine(bishop + rook);
    }
}
