using System;
using System.Linq;

class P1796 {
    static int[] Rouble = new int[] { 10, 50, 100, 500, 1000, 5000 };
    
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = new int[6];
        for (int i = 0; i < 6; ++i) a[i] = int.Parse(line[i]);
        var price = int.Parse(Console.ReadLine());
        var sum = 0;
        for (int i = 0; i < 6; ++i) sum += a[i] * Rouble[i];
        int k = 0;
        while (k < 6 && a[k] == 0) ++k;
        if (k == 6) {
            Console.WriteLine('1');
            Console.WriteLine('0');
        } else {
            var sum1 = sum - Rouble[k];
            var lb = sum1 / price + 1;
            var ub = sum / price;
            Console.WriteLine(ub - lb + 1);
            var ans = Enumerable.Range(lb, ub - lb + 1);
            Console.WriteLine(String.Join(" ", ans));
        }
    }
}
