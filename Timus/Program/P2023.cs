using System;

class P2023 {
    public static void Main() {
        var a = new string[] { null, "APOR", "BMS", "DGJKTW" };
        var b = new int[256];
        for (int i = 1; i <= 3; ++i) 
            foreach (var j in a[i]) b[j] = i;
        var n = int.Parse(Console.ReadLine());
        var current = 1;
        var ans = 0;
        for (int i = 0; i < n; ++i) {
            var newPos = b[Console.ReadLine()[0]];
            ans += Math.Abs(newPos - current);
            current = newPos;
        }
        Console.WriteLine(ans);
    }
}
