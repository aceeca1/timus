﻿using System;

class P2011 {
    static double LogFactorial(int n) {
        double ans = 0.0;
        for (int i = 2; i <= n; ++i) ans += Math.Log(i);
        return ans;
    }

    public static void Main() {
        Console.ReadLine();
        var a = new int[4];
        foreach (var i in Console.ReadLine().Split())
            ++a[int.Parse(i)];
        var u = LogFactorial(a[1] + a[2] + a[3]);
        var u1 = LogFactorial(a[1]);
        var u2 = LogFactorial(a[2]);
        var u3 = LogFactorial(a[3]);
        Console.WriteLine(u - (u1 + u2 + u3) > Math.Log(5.5) ? "Yes" : "No");
    }
}
