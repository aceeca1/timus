using System;

class P1197 {
    static int[] DX = { -1, -2, -2, -1,  1,  2,  2,  1 }; 
    static int[] DY = {  2,  1, -1, -2, -2, -1,  1,  2 };
    
    static int CanAttack(string s) {
        int x = s[0] - 'a';
        int y = s[1] - '1';
        int ans = 0;
        for (int i = 0; i < 8; ++i) {
            int x1 = x + DX[i];
            if (x1 < 0 || 8 <= x1) continue;
            int y1 = y + DY[i];
            if (y1 < 0 || 8 <= y1) continue;
            ++ans;
        }
        return ans;
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            Console.WriteLine(CanAttack(s));
        }
    }
}
