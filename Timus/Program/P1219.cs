using System;

class P1219 {
    const int M = 1000000;
    
    public static void Main() {
        var random = new Random();
        for (int i = 0; i < M; ++i)
            Console.Write((char)('a' + random.Next(26)));
        Console.WriteLine();
    }
}
