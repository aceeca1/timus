using System;
using System.Linq;

class P1636 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var t1 = int.Parse(line[0]);
        var t2 = int.Parse(line[1]);
        line = Console.ReadLine().Split();
        var panelty = 20 * line.Sum(k => int.Parse(k));
        if (t2 - panelty < t1) Console.WriteLine("Dirty debug :(");
        else Console.WriteLine("No chance.");
    }
}
