using System;
using System.Collections.Generic;
using System.Linq;

class P1086 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        var m = a.Max();
        var mm = (int)Math.Ceiling(m * Math.Log(m) * 1.5);
        var prime = new List<int>(new Prime(mm).From(2));
        for (int i = 0; i < n; ++i) Console.WriteLine(prime[a[i] - 1]);
    }
    
    // Snippet: Prime
    class Prime {
        int[] A;
        
        public Prime(int n) {
            A = new int[n + 1];
            int u = 0;
            for (int i = 2; i <= n; ++i) {
                int v = A[i];
                if (v == 0) u = A[u] = v = i;
                for (int w = 2; i * w <= n; w = A[w]) {
                    A[i * w] = w;
                    if (w >= v) break;
                }
            }
            A[u] = n + 1;
        }
        
        public bool Is(int n) { return n >= 2 && A[n] > n; }
        public int Next(int n) { return A[n]; }
        public int MinFactor(int n) { return Math.Min(n, A[n]); }
        
        public IEnumerable<int> From(int n) {
            while (n < A.Length) {
                yield return n;
                n = A[n];
            }
        }
    }
}
