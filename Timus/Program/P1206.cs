using System;
using System.Numerics;

class P1206 {
    public static void Main() {
        var k = int.Parse(Console.ReadLine());
        BigInteger ans = 36;
        for (int i = 1; i < k; ++i) ans *= 55;
        Console.WriteLine(ans);
    }
}
