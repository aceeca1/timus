﻿using System;

class P1404 {
    public static void Main() {
        var s = Console.ReadLine().ToCharArray();
        for (int i = 0; i < s.Length; ++i) s[i] -= 'a';
        for (int i = s.Length - 1; i > 0; --i)
            s[i] = (char)((s[i] - s[i - 1] + 26) % 26);
        s[0] = (char)((s[0] + 21) % 26);
        for (int i = 0; i < s.Length; ++i) s[i] += 'a';
        Console.WriteLine(new string(s));
    }
}
