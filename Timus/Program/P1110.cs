using System;
using System.Linq;
using System.Numerics;

class P1110 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]); 
        var m = int.Parse(line[1]);
        var y = int.Parse(line[2]);
        var range = Enumerable.Range(0, m);
        var ans = range.Where(x => BigInteger.ModPow(x, n, m) == y);
        var ansStr = String.Join(" ", ans);
        Console.WriteLine(ansStr.Length == 0 ? "-1" : ansStr);
    }
}
