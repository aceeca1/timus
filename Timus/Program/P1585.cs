using System;
using System.Collections.Generic;
using System.Linq;

class P1585 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Dictionary<string, int>();
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            int z; a.TryGetValue(s, out z);
            a[s] = z + 1;
        }
        var max = a.Values.Max();
        Console.WriteLine(a.First(ai => ai.Value == max).Key);
    }
}
