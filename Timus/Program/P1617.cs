using System;
using System.Collections.Generic;
using System.Linq;

class P1617 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Dictionary<int, int>();
        for (int i = 0; i < n; ++i) {
            var u = int.Parse(Console.ReadLine());
            int v; a.TryGetValue(u, out v);
            a[u] = v + 1;
        }
        Console.WriteLine(a.Sum(kv => kv.Value >> 2));
    }
}
