using System;
using System.Collections.Generic;

class P1573 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = new Dictionary<string, int>();
        a["Blue"] = int.Parse(line[0]);
        a["Red"] = int.Parse(line[1]);
        a["Yellow"] = int.Parse(line[2]);
        var n = int.Parse(Console.ReadLine());
        int ans = 1;
        for (int i = 0; i < n; ++i) ans *= a[Console.ReadLine()];
        Console.WriteLine(ans);
    }
}
