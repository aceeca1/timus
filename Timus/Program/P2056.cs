using System;
using System.Linq;

class P2056 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(Console.ReadLine());
        if (a.Any(k => k == 3)) Console.WriteLine("None");
        else if (a.All(k => k == 5)) Console.WriteLine("Named");
        else if (a.Average() >= 4.5) Console.WriteLine("High");
        else Console.WriteLine("Common");
    }
}
