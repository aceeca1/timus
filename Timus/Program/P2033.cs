﻿using System;
using System.Collections.Generic;
using System.Linq;

class P2033 {
    class Friend {
        public string Name, Device;
        public int Price;
    }

    static IEnumerable<Friend> ReadAll() {
        for (;;) {
            var name = Console.ReadLine();
            if (name == null) break;
            yield return new Friend {
                Name = name,
                Device = Console.ReadLine(),
                Price = int.Parse(Console.ReadLine())
            };
        }
    }

    public static void Main() {
        var a = ReadAll().GroupBy(k => k.Device);
        var a1 = a.OrderByDescending(kv => kv.Count());
        var a2 = a1.ThenBy(kv => kv.Min(v => v.Price));
        Console.WriteLine(a2.First().Key);
    }
}
