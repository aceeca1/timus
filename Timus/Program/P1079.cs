using System;
using System.Collections.Generic;
using System.Linq;

class P1079 {
    public static void Main() {
        var a = new List<int>();
        for (;;) {
            var line = int.Parse(Console.ReadLine());
            if (line == 0) break;
            a.Add(line);
        }
        var m = a.Max();
        var b = new int[m + 1];
        b[1] = 1;
        for (int i = 2; i <= m; i += 2) {
            b[i] = b[i >> 1];
            if (m < i + 1) break;
            b[i + 1] = b[i >> 1] + b[(i >> 1) + 1];
        }
        var c = new int[m + 1];
        for (int i = 1; i <= m; ++i) c[i] = Math.Max(c[i - 1], b[i]);
        foreach (var i in a) Console.WriteLine(c[i]);
    }
}
