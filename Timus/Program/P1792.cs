using System;

class P1792 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a1 = int.Parse(line[0]);
        var a2 = int.Parse(line[1]);
        var a3 = int.Parse(line[2]);
        var a4 = int.Parse(line[3]);
        var a234 = int.Parse(line[4]);
        var a134 = int.Parse(line[5]);
        var a124 = int.Parse(line[6]);
        var a1234v1 = a1 ^ a234;
        var a1234v2 = a2 ^ a134;
        var a1234v3 = a3 ^ a124;
        var a1234 = a1234v1 + a1234v2 + a1234v3 >= 2 ? 1 : 0;
        if (a1234v1 != a1234) {
            if (a234 != (a2 ^ a3 ^ a4)) a234 = a2 ^ a3 ^ a4;
            else a1 = a1234 ^ a234;
        } else if (a1234v2 != a1234) {
            if (a134 != (a1 ^ a3 ^ a4)) a134 = a1 ^ a3 ^ a4;
            else a2 = a1234 ^ a134;
        } else if (a1234v3 != a1234) {
            if (a124 != (a1 ^ a2 ^ a4)) a124 = a1 ^ a2 ^ a4;
            else a3 = a1234 ^ a124;
        } else a4 = a1234 ^ a1 ^ a2 ^ a3;
        Console.Write(a1);
        Console.Write(' ');
        Console.Write(a2);
        Console.Write(' ');
        Console.Write(a3);
        Console.Write(' ');
        Console.Write(a4);
        Console.Write(' ');
        Console.Write(a234);
        Console.Write(' ');
        Console.Write(a134);
        Console.Write(' ');
        Console.WriteLine(a124);
    }
}
