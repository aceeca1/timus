﻿using System;
using System.Collections.Generic;

class P1654 {
    public static void Main() {
        var s = Console.ReadLine();
        var stack = new Stack<char>();
        for (int i = s.Length - 1; i >= 0; --i)
            if (stack.Count != 0 && s[i] == stack.Peek()) stack.Pop();
            else stack.Push(s[i]);
        Console.WriteLine(String.Concat(stack));
    }
}
