using System;

class P2001 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a1 = int.Parse(line[0]);
        var b1 = int.Parse(line[1]);
        line = Console.ReadLine().Split();
        var a2 = int.Parse(line[0]);
        var b2 = int.Parse(line[1]);
        line = Console.ReadLine().Split();
        var a3 = int.Parse(line[0]);
        var b3 = int.Parse(line[1]);
        var a = a1 - a3;
        var b = b1 - b2;
        Console.Write(a);
        Console.Write(' ');
        Console.WriteLine(b);
    }
}
