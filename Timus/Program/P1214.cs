using System;

class P1214 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var x = int.Parse(line[0]);
        var y = int.Parse(line[1]);
        if (x > 0 && y > 0 && ((x + y) & 1) != 0) {
            Console.Write(y);
            Console.Write(' ');
            Console.WriteLine(x);
        } else {
            Console.Write(x);
            Console.Write(' ');
            Console.WriteLine(y);
        }
    }
}
