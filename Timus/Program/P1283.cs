using System;

class P1283 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var initial = double.Parse(line[0]);
        var die = double.Parse(line[1]);
        var percent = double.Parse(line[2]);
        var a = Math.Log(die) - Math.Log(initial);
        var b = Math.Log((100.0 - percent) / 100.0);
        Console.WriteLine(Math.Max(0, (int)Math.Ceiling(a / b)));
    }
}
