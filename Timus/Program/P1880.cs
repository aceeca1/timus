using System;
using System.Collections.Generic;
using System.Linq;

class P1880 {
    static IEnumerable<int> ReadSeq() {
        Console.ReadLine();
        return Console.ReadLine().Split().Select(k => int.Parse(k));
    }
    
    public static void Main() {
        var a1 = ReadSeq();
        var a2 = ReadSeq();
        var a3 = ReadSeq();
        Console.WriteLine(a1.Intersect(a2).Intersect(a3).Count());
    }
}
