using System;

class P2100 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int ans = 2;
        for (int i = 0; i < n; ++i) {
            ++ans;
            if (Console.ReadLine().EndsWith("+one")) ++ans;
        }
        if (ans == 13) ++ans;
        Console.WriteLine(ans * 100);
    }
}
