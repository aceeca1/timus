using System;
using System.Linq;

class P1582 {
    public static void Main() {
        var a = Console.ReadLine().Split().Select(k => 1.0 / double.Parse(k));
        Console.WriteLine((1000.0 / a.Sum()).ToString("F0"));
    }
}
