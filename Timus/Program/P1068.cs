using System;

class P1068 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine((n + 1) * (Math.Abs(n - 1) + 1) >> 1);
    }
}
