using System;

class P2102 {
    static bool Factor(long n, int k) {
        for (long i = 2; i * i <= n; ++i) {
            if (n < Math.Pow(i, k)) return false;
            while (n % i == 0) {
                n /= i;
                --k;
            }
        }
        if (1L < n) --k;
        return k == 0;
    }

    public static void Main() {
        var n = long.Parse(Console.ReadLine());
        Console.WriteLine(Factor(n, 20) ? "Yes" : "No");
    }
}
