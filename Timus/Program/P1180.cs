﻿using System;
using System.Linq;

class P1180 {
    public static void Main() {
        var m = Console.ReadLine().Sum(k => k - '0') % 3;
        if (m == 0) Console.WriteLine('2');
        else {
            Console.WriteLine('1');
            Console.WriteLine(m);
        }
    }
}
