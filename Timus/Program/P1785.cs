using System;

class P1785 {
    static string Localize(int n) {
        if (n < 5) return "few";
        if (n < 10) return "several";
        if (n < 20) return "pack";
        if (n < 50) return "lots";
        if (n < 100) return "horde";
        if (n < 250) return "throng";
        if (n < 500) return "swarm";
        if (n < 1000) return "zounds";
        return "legion";
    }
    
    public static void Main() {
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine(Localize(n));
    }
}
