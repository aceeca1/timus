using System;
using System.Numerics;

class P1020 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var r = double.Parse(line[1]);
        var a = new Complex[n];
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            a[i] = new Complex(double.Parse(line[0]), double.Parse(line[1]));
        }
        double ans = (a[0] - a[n - 1]).Magnitude;
        for (int i = 1; i < n; ++i)
            ans += (a[i] - a[i - 1]).Magnitude;
        Console.WriteLine((ans + Math.PI * 2 * r).ToString("F2"));
    }
}
