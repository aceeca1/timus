using System;

class P1991 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        int a = 0, b = 0;
        foreach (var i in Console.ReadLine().Split()) {
            var ii = int.Parse(i);
            if (ii > k) a += ii - k;
            else b += k - ii;
        }
        Console.Write(a);
        Console.Write(" ");
        Console.WriteLine(b);
    }
}
