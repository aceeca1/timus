using System;
using System.Linq;

class P1228 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var d = new int[n + 1];
        d[0] = int.Parse(line[1]);
        for (int i = 1; i <= n; ++i) d[i] = int.Parse(Console.ReadLine());
        var ans = Enumerable.Range(1, n).Select(k => d[k - 1] / d[k] - 1);
        Console.WriteLine(String.Join(" ", ans));
    }
}
