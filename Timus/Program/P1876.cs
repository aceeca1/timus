using System;

class P1876 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var ans1 = 39 + 39 * 2 + 1 + ((a - 40) << 1) + 1;
        var ans2 = (b << 1) + 40;
        Console.WriteLine(Math.Max(ans1, ans2));
    }
}
