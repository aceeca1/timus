﻿using System;

class P1194 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        Console.WriteLine((n * (n - 1) >> 1) - k);
    }
}
