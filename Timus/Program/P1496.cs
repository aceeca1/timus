using System;
using System.Collections.Generic;

class P1496 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new Dictionary<string, int>();
        for (int i = 0; i < n; ++i) {
            var s = Console.ReadLine();
            int v; a.TryGetValue(s, out v);
            a[s] = v + 1;
        }
        foreach (var kv in a) 
            if (kv.Value >= 2) Console.WriteLine(kv.Key);
    }
}
