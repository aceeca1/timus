using System;
using System.Collections.Generic;

class P1014 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        if (n == 0) Console.WriteLine("10");
        else if (n == 1) Console.WriteLine('1');
        else {
            var ans = new List<int>();
            for (int i = 9; i >= 2; --i)
                while (n % i == 0) {
                    n /= i;
                    ans.Add(i);
                }
            if (n != 1) Console.WriteLine("-1"); 
            else {
                ans.Reverse();
                Console.WriteLine(String.Concat(ans));
            }
        }
    }
}
