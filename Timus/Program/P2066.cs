using System;
using System.Collections.Generic;
using System.Linq;

class P2066 {
    static IEnumerable<int> Assemble(int a, int b, int c) {
        yield return a + b + c;
        yield return a + b - c;
        yield return a + b * c;
        yield return a - b + c;
        yield return a - b - c;
        yield return a - b * c;
        yield return a * b + c;
        yield return a * b - c;
        yield return a * b * c;
    }
    
    static IEnumerable<int> AssembleAll(int a, int b, int c) {
        yield return Assemble(a, b, c).Min();
        yield return Assemble(a, c, b).Min();
        yield return Assemble(b, a, c).Min();
        yield return Assemble(b, c, a).Min();
        yield return Assemble(c, a, b).Min();
        yield return Assemble(c, b, a).Min();
    }
    
    public static void Main() {
        var a = int.Parse(Console.ReadLine());
        var b = int.Parse(Console.ReadLine());
        var c = int.Parse(Console.ReadLine());
        Console.WriteLine(AssembleAll(a, b, c).Min());
    }
}
