﻿using System;
using System.Numerics;

class P1139 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]) - 1;
        var m = int.Parse(line[1]) - 1;
        var gcd = (int)BigInteger.GreatestCommonDivisor(n, m);
        Console.WriteLine(n + m - gcd);
    }
}
