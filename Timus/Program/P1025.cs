using System;
using System.Linq;

class P1025 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var line = Console.ReadLine().Split();
        var a = line.Select(k => (int.Parse(k) >> 1) + 1);
        Console.WriteLine(a.OrderBy(k => k).Take((n >> 1) + 1).Sum());
    }
}
