﻿using System;
using System.Linq;

class P1642 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var x = int.Parse(line[1]);
        line = Console.ReadLine().Split();
        var a = new int[n];
        for (int i = 0; i < n; ++i) a[i] = int.Parse(line[i]);
        var aE = a.Concat(new int[] { int.MaxValue, int.MinValue });
        var aL = aE.Where(k => k < 0).Max();
        var aR = aE.Where(k => 0 < k).Min();
        if (x < aL || aR < x) Console.WriteLine("Impossible");
        else {
            var bR = x < 0 ? aR + aR - x : x;
            var bL = x < 0 ? -x : x - aL - aL;
            Console.Write(bR);
            Console.Write(' ');
            Console.WriteLine(bL);
        }
    }
}
