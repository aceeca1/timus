using System;
using System.Collections.Generic;

class P1209 {
    static IEnumerable<int> ReadSolve(int n) {
        for (int i = 0; i < n; ++i) {
            var v = double.Parse(Console.ReadLine());
            if (v == 1.0 || v == 2.0) yield return 1;
            else {
                var u = Math.Floor(Math.Sqrt(v + v));
                yield return u * (u + 1.0) * 0.5 + 1.0 == v ? 1 : 0;
            }
        }
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        Console.WriteLine(String.Join(" ", ReadSolve(n)));
    }
}
