using System;

class P1881 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var h = int.Parse(line[0]);
        var w = int.Parse(line[1]);
        var n = int.Parse(line[2]);
        int lines = 0, chars = 0;
        for (int i = 0; i < n; ++i) {
            int len = Console.ReadLine().Length;
            chars += chars == 0 ? len : len + 1;
            if (chars > w) {
                chars = len;
                ++lines;
            }
        }
        if (chars != 0) ++lines;
        Console.WriteLine((lines + h - 1) / h);
    }
}
