using System;

class P1192 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var v = double.Parse(line[0]);
        var a = double.Parse(line[1]);
        var k = double.Parse(line[2]);
        var range = v * v * Math.Sin(Math.PI / 180.0 * 2.0 * a) * 0.1;
        var ans = range / (1.0 - 1.0 / k);
        Console.WriteLine(ans.ToString("F2"));
    }
}
