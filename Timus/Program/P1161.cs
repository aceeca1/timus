﻿using System;
using System.Linq;

class P1161 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var ans = Enumerable.Range(0, n).Select(k => {
            return (double)int.Parse(Console.ReadLine());
        }).OrderByDescending(k => k).Aggregate((k1, k2) => {
            return 2.0 * Math.Sqrt(k1 * k2);
        });
        Console.WriteLine(ans.ToString("F2"));
    }
}
