using System;

class P1924 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        switch (n & 3) {
            case 0: case 3: Console.WriteLine("black"); break;
            default: Console.WriteLine("grimy"); break;
        }
    }
}
