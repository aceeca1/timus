using System;
using System.Linq;

class P1319 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n, n];
        var m = n - 1;
        var k = 1;
        for (int i = -m; i <= m; ++i) {
            int lb = Math.Max(0, i);
            int ub = Math.Min(m, i + m);
            for (int x = lb; x <= ub; ++x) a[x, x - i] = k++;
        }
        for (int i = 0; i < n; ++i) {
            var ai = Enumerable.Range(0, n).Select(kk => a[i, kk]);
            Console.WriteLine(String.Join(" ", ai));
        }
    }
}
