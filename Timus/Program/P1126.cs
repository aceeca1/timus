using System;
using System.Collections.Generic;

class P1126 {
    public static void Main() {
        var m = int.Parse(Console.ReadLine());
        var a = new SlidingMax<int>(m, (k1, k2) => k1 < k2);
        for (int i = 0; i < m; ++i) a.Enqueue(int.Parse(Console.ReadLine()));
        for (;;) {
            Console.WriteLine(a.Max().Data);
            var line = int.Parse(Console.ReadLine());
            if (line == -1) break;
            a.Dequeue();
            a.Enqueue(line);
        }
    }

    // Snippet: SlidingMax
    class SlidingMax<T> {
        public class Element {
            public T Data;
            public int No;
        }

        public Func<T, T, bool> Less;
        public int P = 0, Q = 0;
        Deque<Element> Decreasing;

        public SlidingMax(int cap, Func<T, T, bool> less) {
            Decreasing = new Deque<Element>(cap);
            Less = less;
        }

        public void Enqueue(T data) {
            var newElement = new Element { Data = data, No = Q++ };
            while (Decreasing.Count() != 0 &&
                !Less(newElement.Data, Decreasing.Back().Data))
                Decreasing.PopBack();
            Decreasing.PushBack(newElement);
        }

        public void Dequeue() {
            if (Decreasing.Front().No == P++) Decreasing.PopFront();
        }

        public Element Max() { return Decreasing.Front(); }
    }

    // Snippet: Deque
    class Deque<T> {
        T[] Data;
        int P, Q;

        public Deque(int cap) {
            Data = new T[cap + 1];
            P = 0;
            Q = Data.Length - 1;
        }

        public T Front() { return Data[P]; }
        public T Back()  { return Data[Q]; }
        public void PushFront(T data) { Data[P = Dec(P)] = data; }
        public void PushBack (T data) { Data[Q = Inc(Q)] = data; }
        public void PopFront() { P = Inc(P); }
        public void PopBack () { Q = Dec(Q); }

        public int Count() {
            int ret = Q - P + 1;
            if (ret < 0) ret += Data.Length;
            else if (ret == Data.Length) ret = 0;
            return ret;
        }

        int Inc(int z) { return z != Data.Length - 1 ? z + 1 : 0; }
        int Dec(int z) { return z != 0 ? z - 1 : Data.Length - 1; }
    }
}
