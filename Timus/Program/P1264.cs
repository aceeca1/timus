using System;

class P1264 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        Console.WriteLine(n * (m + 1));
    }
}
