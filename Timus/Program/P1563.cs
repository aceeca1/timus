﻿using System;
using System.Collections.Generic;

class P1563 {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var visited = new HashSet<string>();
        int ans = 0;
        for (int i = 0; i < n; ++i)
            if (!visited.Add(Console.ReadLine())) ++ans;
        Console.WriteLine(ans);
    }
}
