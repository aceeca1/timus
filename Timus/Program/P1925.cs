﻿using System;

class P1925 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var ans = int.Parse(line[1]) - 2;
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            ans += int.Parse(line[0]);
            ans -= int.Parse(line[1]) + 2;
        }
        if (ans < 0) Console.WriteLine("Big Bang!");
        else Console.WriteLine(ans);
    }
}
