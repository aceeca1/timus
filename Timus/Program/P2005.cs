using System;
using System.Linq;

class P2005 {
    class Path {
        public string Name;
        public int Length;
    }
    
    public static void Main() {
        var a = new int[5, 5];
        for (int i = 0; i < 5; ++i) {
            var line = Console.ReadLine().Split();
            for (int j = 0; j < 5; ++j) a[i, j] = int.Parse(line[j]);
        }
        var pathes = new Path[] {
            new Path { 
                Name = "1 2 3 4 5", 
                Length = a[0, 1] + a[1, 2] + a[2, 3] + a[3, 4]
            },
            new Path {
                Name = "1 3 2 4 5",
                Length = a[0, 2] + a[2, 1] + a[1, 3] + a[3, 4]
            },
            new Path {
                Name = "1 3 4 2 5", 
                Length = a[0, 2] + a[2, 3] + a[3, 1] + a[1, 4]
            },
            new Path {
                Name = "1 4 3 2 5",
                Length = a[0, 3] + a[3, 2] + a[2, 1] + a[1, 4]
            }
        };
        var m = pathes.Min(k => k.Length);
        var ans = pathes.First(k => k.Length == m);
        Console.WriteLine(ans.Length);
        Console.WriteLine(ans.Name);
    }
}
