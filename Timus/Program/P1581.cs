using System;
using System.Collections.Generic;
using System.Linq;

class P1581 {
    class PartitionBy: APartitionBy<int, string> {
        class Combiner: ICombiner {
            int value, amount;
            public void Add(int data) { value = data; ++amount; }
            public string Result() {
                return String.Format("{0} {1}", amount, value);
            }
        }
        
        protected override IEnumerable<int> GetInput() { return Input; }
        protected override ICombiner MakeCombiner() { return new Combiner(); }
        protected override bool IsEquivalent(int a1, int a2) { 
            return a1 == a2; 
        }
        
        public IEnumerable<int> Input;
        public PartitionBy(IEnumerable<int> input) { Input = input; }
    }
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var line = Console.ReadLine().Split().Select(k => int.Parse(k));
        Console.WriteLine(String.Join(" ", new PartitionBy(line).Result()));
    }

    // Snippet: PartitionBy
    abstract class APartitionBy<T, U> {
        public interface ICombiner {
            void Add(T data);
            U Result();
        }
    
        protected abstract IEnumerable<T> GetInput();
        protected abstract bool IsEquivalent(T a1, T a2);
        protected abstract ICombiner MakeCombiner();
        
        public IEnumerable<U> Result() {
            var data = default(T);
            var combiner = default(ICombiner);
            foreach (var i in GetInput())
                if (combiner == null) {
                    data = i;
                    combiner = MakeCombiner();
                    combiner.Add(i);
                } else if (IsEquivalent(data, i)) {
                    data = i;
                    combiner.Add(i);
                } else {
                    yield return combiner.Result();
                    data = i;
                    combiner = MakeCombiner();
                    combiner.Add(i);
                }
            if (combiner != null) yield return combiner.Result();
        }
    }
}
