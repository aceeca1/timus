﻿using System;
using System.Collections.Generic;

class P1446 {
    static string[] House = new string[] {
        "Slytherin", "Hufflepuff", "Gryffindor", "Ravenclaw"
    };

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var dict = new Dictionary<string, List<string>>();
        foreach (var i in House) dict[i] = new List<string>();
        for (int i = 0; i < n; ++i) {
            var name = Console.ReadLine();
            dict[Console.ReadLine()].Add(name);
        }
        var head = true;
        foreach (var i in House) {
            if (!head) Console.WriteLine();
            head = false;
            Console.Write(i);
            Console.WriteLine(':');
            foreach (var j in dict[i]) Console.WriteLine(j);
        }
    }
}
